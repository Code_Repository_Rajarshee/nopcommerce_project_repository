/**
 * 
 */
package com.nopcommerce.utility;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * @author Rajarshee
 * Wait methods are defined
 *
 */
public class WaitElement {

	public static void waitForElement(WebElement element, int waitTime, WebDriver driver) {
		
		WebDriverWait wait = new WebDriverWait(driver, waitTime);
		wait.until(ExpectedConditions.elementToBeClickable(element));
	}
	
	public static void waitTill(int waitTill) {
		
		try {
			Thread.sleep(waitTill);
		}
		catch(Exception e) {
			System.out.println(e);
		}
	}
}
