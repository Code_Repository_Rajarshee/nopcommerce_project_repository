/**
 * 
 */
package com.nopcommerce.utility;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

/**
 * @author Rajarshee
 *Browser Methods are defined
 *
 */
public class BrowserFactory {

	static WebDriver driver;
	
	public static WebDriver launchBrowser(String browsername, String URL) {
		
		if(browsername.equalsIgnoreCase("chrome")) {
			System.setProperty("webdriver.chrome.driver", ".\\chromedriver\\chromedriver.exe");
			driver = new ChromeDriver();
		}else if(browsername.equalsIgnoreCase("firefox")) {
			System.setProperty("webdriver.firefox.driver", ".\\firefoxdriver\\geckodriver.exe");
			driver = new FirefoxDriver();
		}else if(browsername.equalsIgnoreCase("IE")) {
			System.setProperty("webdriver.ie.driver", ".\\iedriver\\IEDriverServer.exe");
			driver = new InternetExplorerDriver();
		}else {
			System.out.println("No Browser Selected");
		}	
		driver.manage().window().maximize();
		driver.get(URL);
		return driver;
	}
}
